﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecisionTree
{
    static class Extensions
    {
        public static List<T> Clone<T>(List<T> listToClone)
        {
            List<T> clone = new List<T>();
            foreach (var element in listToClone)
            {
                clone.Add(element);
            }
            return clone;
        }
    }
}
