using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;

namespace DecisionTree
{
    class C45
    {
        bool isItLeaf;
        DataTable dataTable;
        private List<string> classes;
        List<List<string>> attributesList;
        List<List<string>> listOfPassibleAttributes;
        List<string> attributes;
        int bestColumn;
        double priorEntropy;
        Dictionary<string, C45> children;
        string nameOfNode;
        public C45(string pathToXlsxFile)
        {
            nameOfNode = "root";
            //dataTable = XlsxFileWorker.ReadDataExcel("Test.xlsx");
            dataTable = XlsxFileWorker.ReadDataExcel(pathToXlsxFile);
            listOfPassibleAttributes = ExampleTableWorker.GetAttributesList(dataTable);
            classes = ExampleTableWorker.GetClasses(dataTable);
            CreateNode(dataTable);
            FinishTree();
        }

        public C45(DataTable dataTable, string name, List<List<string>> listOfPassibleAttributes, List<string> classes)
        {
            this.classes = classes;
            this.listOfPassibleAttributes = listOfPassibleAttributes;
            nameOfNode = name;
            this.dataTable = dataTable;
            CreateNode(dataTable);
        }

        private void CreateNode(DataTable dataTable)
        {
            children = new Dictionary<string, C45>();
            attributesList = ExampleTableWorker.GetAttributesList(dataTable);
            Dictionary<string, int> instances = ExampleTableWorker.getInstances(dataTable);
            priorEntropy = GetEntropy(instances);
            //Console.WriteLine("priorEntropy = " + priorEntropy);
            if (priorEntropy != 0)
            {
                ExpandTree();
            }
            else
            {
                isItLeaf = true;
                children.Add(instances.Keys.First(), null);
            }
        }

        private void ExpandTree()
        {
            //вместо имени столбца мы берем порядковый номер attributesList
            double maxGain = 0;
            var AttributeWithMaxRemainingEntropies = new Dictionary<string, double>();
            for (int i = 0; i < attributesList.Count; i++)
            {
                var Attribute = new Dictionary<string, double>();
                for (int j = 0; j < attributesList[i].Count; j++)
                {
                    Attribute.Add(attributesList[i][j],
                        GetEntropy(ExampleTableWorker.getInstancesByAttribute(dataTable, i, attributesList[i][j])));
                }
                var gainRatio = GetGainRatio(Attribute, i);
                if (maxGain < gainRatio)
                {
                    maxGain = gainRatio;
                    AttributeWithMaxRemainingEntropies = Attribute;
                    bestColumn = i;
                }
            }
            attributes = listOfPassibleAttributes[bestColumn];
            var cloneListOfPassibleAttributes = Extensions.Clone(listOfPassibleAttributes);
            cloneListOfPassibleAttributes.RemoveAt(bestColumn);
            foreach (var el in AttributeWithMaxRemainingEntropies)
            {
                var newDataTable = dataTable.Copy();
                ExampleTableWorker.CutTable(newDataTable, el.Key, bestColumn);
                //Console.WriteLine(el.Key + " __ " + el.Value);
                children.Add(el.Key, new C45(newDataTable, el.Key, cloneListOfPassibleAttributes, classes));
            }
        }

        private double GetGainRatio(Dictionary<string, double> entropyScores, int columnNumber)
        {
            double infoGain = 0;
            double splitInfo = 0;
            double tmp = 0;
            foreach (var el in entropyScores)
            {
                tmp = ((double)ExampleTableWorker.GetAmountOfSameElementsInColumn(dataTable, columnNumber, el.Key)
                    / dataTable.Rows.Count);
                infoGain += tmp * el.Value;
                splitInfo -= tmp * Math.Log2(tmp);
            }
            infoGain = priorEntropy - infoGain;
            return infoGain / splitInfo;
        }

        private double GetEntropy(Dictionary<string, int> instances)
        {
            double entropy = 0;
            foreach (var el in instances)
            {
                double tmp = (double)el.Value / instances.Values.Sum();
                entropy += -((tmp) * Math.Log2(tmp));
            }
            return entropy;
        }

        private void FinishTree()
        {
            if (!isItLeaf && children.Count < attributes.Count)
            {
                CreateLeaf();
            }
            foreach (var child in children)
            {
                if (child.Value != null)
                {
                    child.Value.FinishTree();
                }
            }
        }

        private void CreateLeaf()
        {
            List<string> results = new List<string>();
            GetFoliage(results);
            children.Add(DetermineLeaf(results), null);
        }

        private void GetFoliage(List<string> results)
        {
            foreach (var child in children)
            {
                foreach (var element in classes)
                {
                    if (children.ContainsKey(element))
                    {
                        results.Add(element);
                    }
                }
                if (null != child.Value)
                {
                    child.Value.GetFoliage(results);
                }
            }
        }
        private string DetermineLeaf(List<string> results)
        {
            var res = results.GroupBy(x => x)
            .Select(x => new { Value = x.Key, Count = x.Count() })
            .OrderByDescending(x => x.Count)
            .First();
            return res.Value;
        }

        public string DetermineResult(List<string> states)
        {
            var parameters = Extensions.Clone(states);
            return getLeaf(parameters);
        }
        private string getLeaf(List<string> states)
        {
            if ((states.Count == 0 || states == null) && !isItLeaf)
            {
                throw new InputDataException("incorrect length");
            }
            if (isItLeaf || (attributes.Contains(states[bestColumn]) && !children.ContainsKey(states[bestColumn])))
            {
                string result = classes.FirstOrDefault(element => children.ContainsKey(element));
                if (result != null)
                {
                    return result;
                }
                else throw new AlgorithmException("Leaf was not created");
            }
            else
            {
                if (attributes.Contains(states[bestColumn]) && children.ContainsKey(states[bestColumn]))
                {
                    C45 child = children[states[bestColumn]];
                    states.RemoveAt(bestColumn);
                    try
                    {
                        return child.getLeaf(states);
                    }
                    catch (InputDataException ex)
                    {
                        throw new InputDataException(ex.Message);
                    }
                }
                else
                {
                    throw new InputDataException("incorrect input");
                }
            }
        }

    }
}
