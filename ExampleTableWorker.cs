﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DecisionTree
{
    static class ExampleTableWorker
    {
        public static int GetAmountOfSameElementsInColumn(DataTable dataTable, int columnNumber, string element)
        {
            int amountOfSameElements = 0;
            foreach (DataRow dataRow in dataTable.Rows)
            {
                if (((string)(dataRow.ItemArray[columnNumber]) == element))
                {
                    amountOfSameElements++;
                }
            }
            //Console.WriteLine(amountOfSameElements);
            return amountOfSameElements;
        }
        public static List<string> GetClasses(DataTable dataTable)
        {
            var classes = new List<string>();
            foreach (DataRow dataRow in dataTable.Rows)
            {
                var clazz = (string)(dataRow.ItemArray[dataTable.Columns.Count - 1]);
                if (!classes.Contains(clazz))
                {
                    classes.Add(clazz);
                }
            }
            return classes;
        }
        public static List<List<string>> GetAttributesList(DataTable dataTable)
        {
            List<string> tmp;
            var attributesList = new List<List<string>>();
            for (int i = 0; i < dataTable.Columns.Count - 1; i++)
            {
                tmp = new List<string>();
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    if (!tmp.Contains((string)(dataRow.ItemArray[i])))
                    {
                        tmp.Add((string)(dataRow.ItemArray[i]));
                    }
                }
                attributesList.Add(tmp);
            }
            return attributesList;
        }
        public static Dictionary<string, int> getInstancesByAttribute(DataTable dataTable, int columnNumber,
            string attribute)//этот метод почти такой же, как и метод ниже
        {
            var instances = new Dictionary<string, int>();
            foreach (DataRow dataRow in dataTable.Rows)
            {
                if (attribute.Equals((string)(dataRow.ItemArray[columnNumber])))
                {
                    var instance = (string)(dataRow.ItemArray[dataTable.Columns.Count - 1]);
                    if (!instances.ContainsKey(instance))
                    {
                        instances.Add(instance, 1);
                    }
                    else
                    {
                        instances[instance]++;
                    }
                }
            }
            return instances;
        }
        public static Dictionary<string, int> getInstances(DataTable dataTable)
        {
            var instances = new Dictionary<string, int>();
            foreach (DataRow dataRow in dataTable.Rows)
            {
                var instance = (string)(dataRow.ItemArray[dataTable.Columns.Count - 1]);
                if (!instances.ContainsKey(instance))
                {
                    instances.Add(instance, 1);
                }
                else
                {
                    instances[instance]++;
                }
            }
            return instances;
        }

        public static void CutTable(DataTable dataTable, string value, int columnNumber)
        {
            List<DataRow> deleteIt = new List<DataRow>();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                var dataRow = dataTable.Rows[i];
                if (!((string)dataRow.ItemArray[columnNumber]).Equals(value))
                {
                    deleteIt.Add(dataRow);
                }
            }
            foreach (var el in deleteIt)
            {
                dataTable.Rows.Remove(el);
            }
            dataTable.Columns.RemoveAt(columnNumber);
        }
    }
}
