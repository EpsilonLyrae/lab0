﻿using System;
using System.Collections.Generic;

namespace DecisionTree
{
    class DataSetException : Exception
    {
        public DataSetException(string message) : base(message)
        { }
    }
    class AlgorithmException : Exception
    {
        public AlgorithmException(string message) : base(message)
        { }
    }
    class InputDataException : Exception
    {
        public InputDataException(string message) : base(message)
        { }
    }


    class Program
    {
        static void Main(string[] args)
        {
            var root = new C45("Test.xlsx");
            try
            {
                var example = new List<string>
            {
                "Sun", "Hot", "High", "High" 
            };
                Console.WriteLine(root.DetermineResult(example));
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
        }
    }
}
